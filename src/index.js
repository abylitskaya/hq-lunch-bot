import { Telegraf as TelegramBot } from 'telegraf';
import { Keyboard } from 'telegram-keyboard';
import fetch from 'node-fetch';

const token = '5027876848:AAGqFBtmzG84wR5xm_mmvh4ELYyYBu4U-Jo';

const lunchButton = 'Кушааааать';
const topicButton = 'Что обсудим?';

const bot = new TelegramBot(token, { polling: true });

console.log('Bot started');

bot.start((ctx) => {
  ctx.reply('Уиииии');
  ctx.reply('Кнопаськи добавлены!');

  return ctx.reply('Чего хотите?', Keyboard.make([lunchButton, topicButton]).builtIn());
});

const messages = [
  { text: 'Кушать, котаны' },
  { sticker: 'CAACAgIAAxkBAAM8YefhhIXbmLrN2F5twt3ziismbkQAAo8SAALo1uISd--yCFMOy_QjBA' },
  { text: 'Го обедать' },
  { sticker: 'CAACAgIAAxkBAANCYefin7-A7OO5NXzkTTW6we68eGsAAi0AA0f7CQyCLHF8GCSy8yME' },
  { text: 'Кушаааааааааать (с) Ангелинааааа' },
  { sticker: 'CAACAgIAAxkBAAM9YefhighqOe5gIMFI052sAmcICOIAAksAA-jtChCQlUTDF6SkfyME' },
  { text: 'На низком старте' },
  { sticker: 'CAACAgIAAxkBAAM-YefhqT6msFVwupFsmNpGgM9acb8AAk0AAygPahSXixVOpnGrYiME' },
  { text: 'Я вниз и через мин 5 можно (с) Максимка' },
  { sticker: 'CAACAgIAAxkBAAM_Yefh9U36DpcPrtu8yIw4UKV8qr4AAsoAA8D7CAABiWjFgNRSnsUjBA' },
  { text: 'а кто вообще обедать то пойдет?' },
  { sticker: 'CAACAgIAAxkBAANAYefia3BLFsgZtg3Ss-Tlj7F7FCsAAj4AA3vAfROBBrz0y2KGNiME' },
  { text: 'Всех позвали, а меня снова нет (с) Лиза' },
  { sticker: 'CAACAgIAAxkBAANBYefieKMUefBh7YRiI_QiK0-JA7gAAvUCAAJcAmUDh9-mWqxXsIcjBA' },
  { text: 'Потихоньку подтягиваемся (с) Катя' },
];

bot.hears(lunchButton, (ctx) => {
  const index = Math.floor(Math.random() * 100) % messages.length;
  const reply = messages[index];

  if (reply.sticker) {
    ctx.replyWithSticker(reply.sticker);
  }

  if (reply.text) {
    ctx.reply(reply.text);
  }
});

bot.hears(topicButton, async (ctx) => {
  try {
    const response = await fetch('https://randomall.ru/api/custom/gens/1888', { method: 'POST' });
    const { msg } = await response.json();
    ctx.reply(msg);
  } catch (e) {
    console.log(e);
    ctx.reply('Что-то пошло не так -_-');
  }
});

bot.on('sticker', (ctx) => {
  console.log(ctx.update.message.sticker.file_id);
});

bot.launch();
